var FlashCalendar = FlashCalendar || {};

FlashCalendar.params = {
  'target' : '#calendar-body',
  'phpLoader' : 'fc/load/node/',
};

FlashCalendar.loadNode = function(nids) {

nid_string = new String();

if (nids.length > 1) {
  for (nid in nids) {
   nid_string += nids[nid] + '-';
  }
} else {
  nid_string = nids;
}

//alert(' I got called and here are the nids: ' + nids);
  
path = FlashCalendar.params.phpLoader + nid_string;
  
jQuery.post(path,
    {'nids' : nids}, 
    function(data){  
      $(FlashCalendar.params.target).empty().append(data).hide().fadeIn("SLOW");
    }
  );
}
